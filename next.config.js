const withPlugins = require("next-compose-plugins");

const nextConfig = {
  trailingSlash: true,
  i18n: {
    locales: ['en', 'de'],
    defaultLocale: 'en',
  },
}


const rewrites = {
  async rewrites() {
    return [
      {
        source: '/en/.json',
        destination: '/en.json',
      },
      {
        source: '/.json',
        destination: '/en.json',
      },
      {
        source: '/_next/data/:third/en/.json',   //doesn't work
        destination: '/another',
      },
      {
        source: '/_n2ext/data/:third/en/.json',  // WORKS
        destination: '/another',
      },
      {
        source: '/_next/en/.json',
        destination: '/another',
      },

      {
        source: '/_next/data/:path*', //doesn't work
        destination: '/another/',
      },
      {
        source: '/_n2ext/data/:path*', //doesn't work
        destination: '/another/',
      },
      {
        source: '/n2ext/data/:path*', //doesn't work
        destination: '/another/',
      },
      {
        source: '/next/data/:path*', //doesn't work
        destination: '/another/',
      },
      {
        source: '/_next/data/',   //doesn't work
        destination: '/another/',
      },
      {
        source: '/_n2ext/data/',  //works
        destination: '/another/',
      },
      {
        source: '/next/data/:path*',  //doesn't work
        destination: '/another/',
      },
      {
        source: '/some/thing/',   //works
        destination: '/another/',
      },
      {
        source: '/some/thing/:path*',
        destination: '/another/',
      },
      {
        source: '/_some/thing/',  //works
        destination: '/another/',
      },
      {
        source: '/testtest/',
        destination: '/another',
      },
    ]
  }
}

module.exports = withPlugins(
  [rewrites],
  nextConfig,
)

