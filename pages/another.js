import React from "react";
import Link from "next/link";

function Another() {
  return (
    <div>
        another page<br/>
        <Link href={'/subpage'}><a>go to subpage</a></Link>
    </div>
  )
}

export default Another
